# shuttle workloads

## Helpful commands ✨

After a while, you might run into an error like `failed to create fsnotify watcher: too many open files` when trying to view k8s logs.
Then it's helpful to use a tool like [inotify-info](https://github.com/mikesart/inotify-info.git) which can show you if you ran into a limit.
(You can properly also view this in Grafana, but in my case, Grafana also gets very slow, so this was easier 😇)

Anyhow, by default on `Debian GNU/Linux 12 (bookworm)` on my machine it was set to:

```sh
# sysctl fs.inotify
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 128
fs.inotify.max_user_watches = 123745
```

The problem was I ran into `fs.inotify.max_user_instances`, so I increased it to `1024` (state on 2024-05-18):

```sh
# sysctl -w fs.inotify.max_user_instances=1024
```

## Nice to know 🤯

1) Does my pv survive when I rename an ArgoCD application?

Nope - it will get deleted and created again, even though nothing, but the application name changed - at least when you do it as in [this commit](https://gitlab.com/schober-server/kubernetes/workloads/shuttle/-/commit/9a373c013a51561602e31694b326f8f33cdec55e)

