#!/bin/sh
# Make sure to add helm repo: helm repo add mariadb-operator https://mariadb-operator.github.io/mariadb-operator

helm show values mariadb-operator/mariadb-operator > values.yaml
