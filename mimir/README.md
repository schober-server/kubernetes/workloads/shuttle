# Mimir Distr. Workloads

If you are facing problems, when getting data mimir, try out testing it out like this:
```sh
k exec -it -n mimir deployments/mimir-nginx -- curl -H "X-Scope-OrgID: 2tefan" http://mimir-query-frontend.mimir.svc.cluster.local:8080/prometheus/api/v1/label/__name__/values
```

If you getting something, like to many unhealthy instances in ring, then check the logs of _all_ containers. Most likely you will find something like this:
```log
ts=2024-10-22T22:20:58.594311848Z caller=memberlist_client.go:615 level=warn phase=startup msg="joining memberlist cluster" attempts=2 max_attempts=10 err="found no nodes to join"
ts=2024-10-22T22:21:01.294054125Z caller=resolver.go:87 level=error msg="failed to lookup IP addresses" host=mimir-gossip-ring.mimir.svc.cluster.local. err="lookup mimir-gossip-ring.mimir.svc.cluster.local. on 10.43.0.10:53: no such host"
```

This is fixed with an restart of that pod.

Also when pushing data with for example alloy, make you push to `/api/v1/push` and _not_ `/api/v1/write`. This was quite confusing when migrating from classic prometheus.
Last tip: Whenever troubleshooting some, just checkout the `nginx.conf`, which could help, when the documentation is lacking 🧠:

```sh
k exec -it -n mimir deployments/mimir-nginx -- cat /etc/nginx/nginx.conf
```

