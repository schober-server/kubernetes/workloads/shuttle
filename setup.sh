#!/bin/bash

set -eu

wait_for_secret() {
  SECRET_NAME=$1
  NAMESPACE=$2
  RETRIES=0
  SECRET_BASE64=$(kubectl get secret $SECRET_NAME -n $NAMESPACE -o jsonpath='{.data.password}')

  while [ $? -ne 0 ]; do
    echo >&2 "Waiting for secret to become available... ($RETRIES)"
    sleep $(( RETRIES++ ))
    SECRET_BASE64=$(kubectl get secret $SECRET_NAME -n $NAMESPACE -o jsonpath='{.data.password}')
  done

  echo "$SECRET_BASE64"
}

wait_for_namespace() {
  NAMESPACE=$1
  RETRIES=0

  while ! kubectl get namespaces "$NAMESPACE"; do
    echo >&2 "Waiting for namespace to become available... ($RETRIES)"
    sleep $(( RETRIES++ ))
  done
}

generate_password() {
  head /dev/urandom | tr -dc A-Za-z0-9 | head -c 64 ; echo ''
}

# generate_nextcloud_secret() {
#   wait_for_namespace nextcloud
#   echo "Creating secret for nextcloud"
#   kubectl create secret generic mariadb-nextcloud-secret -n database \
#       --from-literal=username=nextcloud \
#       --from-literal=password="$(generate_password)"
#
#   kubectl annotate -n database secrets mariadb-nextcloud-secret \
#       reflector.v1.k8s.emberstack.com/reflection-allowed="true" \
#       reflector.v1.k8s.emberstack.com/reflection-allowed-namespaces="nextcloud" \
#       reflector.v1.k8s.emberstack.com/reflection-auto-enabled="true" \
#       reflector.v1.k8s.emberstack.com/reflection-auto-namespaces="nextcloud"
#
#   kubectl create secret generic redis-secret -n nextcloud \
#       --from-literal=password="$(generate_password)"
# }

map_ports_locally() {
  echo "Mapping port localally 8080 -> 80"
  echo "                       8443 -> 443"
  kubectl wait --namespace ingress-nginx \
          --for=condition=ready pod \
          --selector=app.kubernetes.io/component=controller \
          --timeout=120s
  kubectl port-forward -n ingress-nginx svc/ingress-nginx-controller 8080:80 &
  kubectl port-forward -n ingress-nginx svc/ingress-nginx-controller 8443:443 &
}

print_overview() {
  INITIAL_ADMIN_SECRET=$(wait_for_secret argocd-initial-admin-secret $K_NS_ARGOCD | base64 --decode)
  ARGOCD_HOST=$(kubectl get ingress -n argocd ingress-argocd -o jsonpath='{.spec.rules[0].host}')

  echo "ArgoCD is up and running 🎉"
  echo "If you want to access it, you can do it either by CLI:"
  echo "  '$ARGOCD ...'"
  echo "Or via the ingress:"
  echo "  'https://$ARGOCD_HOST:8443'"
  echo "Default login:"
  echo "  Username: 'admin'"
  echo "  Password: '$INITIAL_ADMIN_SECRET'"
}

: "${USE_MINIKUBE=true}"
: "${K_NS_ARGOCD:=argocd}"
: "${ARGOCD:=kubectl exec deployments/argocd-server -n $K_NS_ARGOCD -- argocd}"

if ! command -v kubectl &> /dev/null ; then
  echo "Please setup your kubernetes env first!"
  exit 1
fi

if ! kubectl config view --minify -o jsonpath='{..namespace}' >/dev/null 2>&1 ; then
  echo "No namespaces found..."
  exit 1
fi


while [[ $# -gt 0 ]]; do
  case $1 in
    -r|--reattach)
      map_ports_locally
      print_overview
      exit 0
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      shift # past argument
      ;;
  esac
done

echo "+--------------------------- Installing ArgoCD ---------------------------+"
kubectl apply -k argocd/
kubectl apply -f argocd/shuttle-application-set.yaml

echo ""
echo "+--------------------------- Directly applying base workloads ---------------------------+"
kubectl kustomize database/      --enable-helm | kubectl apply -f -
kubectl kustomize ingress-nginx/ --enable-helm | kubectl apply -f -

# echo ""
# echo "+--------------------------- Generate secrets ---------------------------+"
# generate_borg_secret &
# generate_nextcloud_secret &
# generate_photoprism_secret &
# generate_mariadb_secrets &
# generate_template_gitlab_runner_secret &
# generate_grafana_secret &

#echo "Waiting for all pods to be ready..."
#kubectl wait --for=condition=Ready pods --all --all-namespaces --timeout=5m

if [ "$USE_MINIKUBE" == "true" ]; then
  map_ports_locally
fi

echo "All secrets have to be manually added/restore from vault!"

print_overview

