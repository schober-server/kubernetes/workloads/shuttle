#!/bin/bash

possible_empty_files=$(find . -size 0 | grep -E -v '\.[0-9]{5}\.(jpg|heic|png|mov|mp4)' | sed 's#\.[a-z0-9]*$##g')

declare -i good_files=0
declare -i bad_files=0

for possible_empty_file in $possible_empty_files; do
  #echo "Searching for $possible_empty_file"
  if find . -iwholename "${possible_empty_file}*" -size +1c -exec false {} +; then
    echo "Sorry, ${possible_empty_file} is properly lost to time ;("
    bad_files+=1
  else
    good_files+=1
  fi
done

echo "Summary files: bad: ${bad_files} // good: ${good_files}"

